<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});


/*Sentinel Authentication*/
// Route::group(['prefix' => 'user', 'middleware' => 'user', 'as' => 'user'], function(){

// });

Route::get('/register', 'RegistrationController@register');
Route::post('/register', 'RegistrationController@postRegister');
Route::get('/login', 'LoginController@login');
Route::post('/login', 'LoginController@postLogin');
Route::post('/logout', 'LoginController@logout');
//admin route
Route::group(['prefix' => 'admin', 'middleware' => 'admin', 'as' => 'admin.'], function () {
	Route::get('/',  array('as' => 'index','uses' => 'AdminController@index'));
});

//user route
Route::group(['prefix' => 'user', 'middleware' => 'user', 'as' => 'user.'], function () {
	Route::get('/',  array('as' => 'index','uses' => 'UserController@index'));
	Route::get('/profile',  array('as' => 'profile', 'uses' => 'UserController@profile'));
	Route::post('/profile', array('as' => 'profile', 'uses' => 'UserController@updateProfile'));

});