@extends('default.layout')

@section('title', 'Home')


@section('pages_styles')
    <style type="text/css">
        .container{
            height: 600px;
        }
        .content{
            height: 400px;
        }
    </style>
@endsection

@section('top')

@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-lg-center mt-5">
            <div class="col col-lg-6">
              <div class="content card">
                <div class="card-block">
                  <h1 class="card-title text-center text-primary mt-200">PrintingPress System</h1>
                  <p class="card-text"></p>
                  <a href="{{ url('/login') }}" class="card-link btn btn-info">Login</a>
                  <a href="{{ url('/register') }}" class="card-link btn btn-primary">Registration</a>
                </div>
              </div>
            </div>
        </div>
    </div>
@endsection

@section('pages_scripts')

@endsection