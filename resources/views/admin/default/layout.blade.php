<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title> @yield('title')|Admin|PP </title>
        <meta name="description" content="">
        <meta name="robots" content="all,follow">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- Core Styles -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">

       
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="{{ asset('assets/admin/css/style.default.css') }}" id="theme-stylesheet">
        <!-- jQuery Circle-->
        <link rel="stylesheet" href="{{ asset('assets/admin/css/grasp_mobile_progress_circle-1.0.0.min.css') }}">        
        <!-- Favicon-->
        <link rel="shortcut icon" href="{{ asset('assets/admin/images/favicon.ico') }}">
        <!-- Font Icons CSS-->
        <link rel="stylesheet" href="https://file.myfontastic.com/da58YPMQ7U5HY8Rb6UxkNf/icons.css">
        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
         <!-- page level css -->
         
        @yield('pages_styles')
    </head>

    <body>
       <!-- Side Navbar -->
        <nav class="side-navbar">
          <div class="side-navbar-wrapper">
            <div class="sidenav-header d-flex align-items-center justify-content-center">
              <div class="sidenav-header-inner text-center">
                <a href="#"><img src="{{ asset('assets/admin/images/avatar.png') }}" alt="person" class="img-fluid rounded-circle"></a>
                <h2 class="h5 text-uppercase">Anderson Hardy</h2>
                <span class="text-uppercase">Admin</span>
              </div>
              <div class="sidenav-header-logo"><a href="{{ url('admin/') }}" class="brand-small text-center"> <strong>P</strong><strong class="text-primary">P</strong></a></div>
            </div>
            <div class="main-menu">
              <ul id="side-main-menu" class="side-menu list-unstyled">                  
                <li class="{{ url()->current()==url('admin/')?'active':'' }}"><a href="{{ url('admin/') }}"> <i class="icon-home"></i><span>Home</span></a></li>
                <li> <a href="#pages-nav-list" data-toggle="collapse" aria-expanded="false"><i class="icon-interface-windows"></i><span>Dropdown</span>
                    <div class="arrow pull-right"><i class="fa fa-angle-down"></i></div></a>
                  <ul id="pages-nav-list" class="collapse list-unstyled">
                    <li> <a href="#">Page 1</a></li>
                    <li> <a href="#">Page 2</a></li>
                    <li> <a href="#">Page 3</a></li>
                    <li> <a href="#">Page 4</a></li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </nav>
    <div class="page home-page">
        <header class="header">
            <nav class="navbar">
              <div class="container-fluid">
                <div class="navbar-holder d-flex align-items-center justify-content-between">
                  <div class="navbar-header"><a id="toggle-btn" href="#" class="menu-btn"><i class="icon-bars"> </i></a><a href="{{ url('admin/') }}" class="navbar-brand">
                      <div class="brand-text hidden-sm-down"><span>admin </span><strong class="text-primary">Dashboard</strong></div></a></div>
                  <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                    <li class="nav-item dropdown"> <a id="notifications" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><i class="fa fa-bell"></i><span class="badge badge-warning">12</span></a>
                      <ul aria-labelledby="notifications" class="dropdown-menu">
                        <li><a rel="nofollow" href="#" class="dropdown-item"> 
                            <div class="notification d-flex justify-content-between">
                              <div class="notification-content"><i class="fa fa-envelope"></i>You have 6 new messages </div>
                              <div class="notification-time"><small>4 minutes ago</small></div>
                            </div></a></li>
                        <li><a rel="nofollow" href="#" class="dropdown-item"> 
                            <div class="notification d-flex justify-content-between">
                              <div class="notification-content"><i class="fa fa-twitter"></i>You have 2 followers</div>
                              <div class="notification-time"><small>4 minutes ago</small></div>
                            </div></a></li>
                        <li><a rel="nofollow" href="#" class="dropdown-item"> 
                            <div class="notification d-flex justify-content-between">
                              <div class="notification-content"><i class="fa fa-upload"></i>Server Rebooted</div>
                              <div class="notification-time"><small>4 minutes ago</small></div>
                            </div></a></li>
                        <li><a rel="nofollow" href="#" class="dropdown-item"> 
                            <div class="notification d-flex justify-content-between">
                              <div class="notification-content"><i class="fa fa-twitter"></i>You have 2 followers</div>
                              <div class="notification-time"><small>10 minutes ago</small></div>
                            </div></a></li>
                        <li><a rel="nofollow" href="#" class="dropdown-item all-notifications text-center"> <strong> <i class="fa fa-bell"></i>view all notifications                                            </strong></a></li>
                      </ul>
                    </li>
                    <li class="nav-item dropdown"> <a id="messages" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><i class="fa fa-envelope"></i><span class="badge badge-info">10</span></a>
                      <ul aria-labelledby="notifications" class="dropdown-menu">
                        <li><a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                            <div class="msg-profile"> <img src="img/avatar-1.jpg" alt="..." class="img-fluid rounded-circle"></div>
                            <div class="msg-body">
                              <h3 class="h5">Jason Doe</h3><span>sent you a direct message</span><small>3 days ago at 7:58 pm - 10.06.2014</small>
                            </div></a></li>
                        <li><a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                            <div class="msg-profile"> <img src="img/avatar-2.jpg" alt="..." class="img-fluid rounded-circle"></div>
                            <div class="msg-body">
                              <h3 class="h5">Frank Williams</h3><span>sent you a direct message</span><small>3 days ago at 7:58 pm - 10.06.2014</small>
                            </div></a></li>
                        <li><a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                            <div class="msg-profile"> <img src="img/avatar-3.jpg" alt="..." class="img-fluid rounded-circle"></div>
                            <div class="msg-body">
                              <h3 class="h5">Ashley Wood</h3><span>sent you a direct message</span><small>3 days ago at 7:58 pm - 10.06.2014</small>
                            </div></a></li>
                        <li><a rel="nofollow" href="#" class="dropdown-item all-notifications text-center"> <strong> <i class="fa fa-envelope"></i>Read all messages    </strong></a></li>
                      </ul>
                    </li>
                    <li class="nav-item">
                        <form action="{{ url('/logout') }}" method="POST" id="logout-form">
                            {{ csrf_field() }}
                            <a href="#" class="nav-link logout" onclick="document.getElementById('logout-form').submit()">
                                Logout<i class="fa fa-sign-out"></i>
                            </a>
                        </form>
                    </li>
                  </ul>
                </div>
              </div>
            </nav>
        </header>

        @yield('breadcrump')
        @yield('content')
    </div>


        <!-- Core Scripts -->
        <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>



        <!-- page level scripts -->
        @yield('pages_scripts')
    </body>
</html>