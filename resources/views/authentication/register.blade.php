@extends('default.layout')

@section('title', 'Registration')


@section('pages_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/front-end/css/login-register.css') }}">
@endsection

@section('top')

@endsection

@section('content')

    <div class="page login-page">
      <div class="container">
        <div class="form-outer text-center d-flex align-items-center">
          <div class="form-inner">
            <div class="logo text-uppercase"><span>Printing</span><strong class="text-primary">Press</strong></div>
                    @if(session('error'))
                  		<div class="alert alert-danger" role="alert">
					  		<strong>{{ session('error') }}</strong>
						</div>
					@endif
						<div class="reg-error-msg">
							@if (count($errors)>0)
				              <ul class="alert alert-danger">
				                  @foreach($errors->all() as $error)
				                      <li>{{ $error }}</li>
				                  @endforeach
				              </ul>
				         	 @endif
						</div>
		            <form id="login-form" action="{{ url('/register') }}" method="post">
		              {{ csrf_field() }}
		              <div class="form-group">
		                <label for="register-first_name" class="label-custom">First Name</label>
		                <input id="register-first_name" type="text" name="first_name">
		              </div>
		              <div class="form-group">
		                <label for="register-last_name" class="label-custom">Last Name</label>
		                <input id="register-last_name" type="text" name="last_name">
		              </div>
		              <div class="form-group">
		                <label for="register-email" class="label-custom">Email</label>
		                <input id="register-email" type="email" name="email">
		              </div>
		              <div class="form-group">
		                <label for="register-password" class="label-custom">Password</label>
		                <input id="register-password" type="password" name="password">
		              </div>
		              <div class="form-group">
		                <label for="register-password_confirmation" class="label-custom">Confirm Password</label>
		                <input id="register-password_confirmation" type="password" name="password_confirmation">
		              </div>
		              
		              <button type="submit" class="btn btn-primary mr-5">Register</button>
		            </form>
		            <small>Already have an account? </small><a href="{{ url('/login') }}" class="signup">Login</a>
          </div>
          <div class="copyrights text-center">
            <p>Design by <a href="{{ url('/')}}" class="external">ShikderItSolution</a></p>
            <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
          </div>
        </div>
      </div>
    </div>
@endsection

@section('pages_scripts')
        <script src="{{ asset('assets/user/js/jquery.cookie.js') }}"> </script>
        <script src="{{ asset('assets/user/js/grasp_mobile_progress_circle-1.0.0.min.js') }}"></script>
        <script src="{{ asset('assets/user/js/jquery.nicescroll.min.js') }}"></script>
        <script src="{{ asset('assets/user/js/jquery.validate.min.js') }}"></script>
        <script src="{{ asset('assets/user/js/front.js') }}"></script>
       
@endsection