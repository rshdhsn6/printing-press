@extends('default.layout')

@section('title', 'Login')


@section('pages_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/front-end/css/login-register.css') }}">
@endsection

@section('top')

@endsection

@section('content')
   
    <div class="page login-page">
      <div class="container">
        <div class="form-outer text-center d-flex align-items-center">
          <div class="form-inner">
            <div class="logo text-uppercase"><span>Printing</span><strong class="text-primary">Press</strong></div>
                  @if(session('error'))
                    <div class="alert alert-danger" role="alert">
                       <strong>Error: </strong><small>{{ session('error') }}</small>
                   </div>
                   @endif
                   @if(session()->has('msg'))
                  <div class="alert alert-success success-msg">
                    <strong>Success: </strong><small> {{ session()->get('msg') }}</small>
                  </div>
                  @endif
            <form id="login-form" action="{{ url('/login') }}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <label for="login-username" class="label-custom">Email</label>
                <input id="login-username" type="email" name="email">
              </div>
              <div class="form-group">
                <label for="login-password" class="label-custom">Password</label>
                <input id="login-password" type="password" name="password">
              </div>
                <input name="remember_me" id="remember_me" type="checkbox" class="form-control-custom">
                <label for="remember_me">Remember Me</label>
              <button type="submit" class="btn btn-primary mr-5">Login</button>
              <!-- This should be submit button but I replaced it with <a> for demo purposes-->
            </form>

              <a href="#" class="forgot-pass">Forgot Password?</a><small>Do not have an account? </small>
              <a href="{{ url('/register') }}" class="signup">Register</a>
          </div>
          <div class="copyrights text-center">
            <p>Design by <a href="{{ url('/')}}" class="external">ShikderItSolution</a></p>
            <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
          </div>
        </div>
      </div>
    </div>
@endsection

@section('pages_scripts')
        <script src="{{ asset('assets/user/js/jquery.cookie.js') }}"> </script>
        <script src="{{ asset('assets/user/js/grasp_mobile_progress_circle-1.0.0.min.js') }}"></script>
        <script src="{{ asset('assets/user/js/jquery.nicescroll.min.js') }}"></script>
        <script src="{{ asset('assets/user/js/jquery.validate.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
        <script src="{{ asset('assets/user/js/charts-home.js') }}"></script>
        <script src="{{ asset('assets/user/js/front.js') }}"></script>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID.-->
        <!---->
        <script>
          (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
          function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
          e=o.createElement(i);r=o.getElementsByTagName(i)[0];
          e.src='//www.google-analytics.com/analytics.js';
          r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
          ga('create','UA-XXXXX-X');ga('send','pageview');
        </script>
@endsection