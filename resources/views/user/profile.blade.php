@extends('user.default.layout')

@section('title', 'Profile')


@section('pages_styles')
   <link rel="stylesheet" href="{{ asset('assets/user/css/profile.css') }}">
@endsection

@section('breadcrump')
 <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ url('user/') }}">Home</a></li>
    <li class="breadcrumb-item active"><small>Profile</small></li>

  </ol>
    @if(session()->has('success'))
          <p class="alert alert-success">
            <small>{{ session()->get('success') }}</small>
          </p>
    @endif
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#currentPreferences">Profile</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#alternative"> Alternative </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#link">Link</a>
                </li>
                
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="currentPreferences">
                    <form  action="{{ url('user/profile') }}" method="POST" class="form-horizontal mt-5">
                      {{ csrf_field() }}
                      <div class="row">
                        <div class="col-6">
                          <div class="form-group row">
                            <label class="col-sm-3 form-control-label">First Name</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" name="first_name" id="first_name" value="{{ $user->first_name }}">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 form-control-label">Last Name</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" name="last_name" id="last_name" value="{{ $user->last_name }}">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 form-control-label">Email</label>
                            <div class="col-sm-9">
                              <input type="email" class="form-control" name="email" id="email" value="{{ $user->email }}">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 form-control-label">Password</label>
                            <div class="col-sm-9">
                              <input type="password" class="form-control" name="password" id="password">
                            </div>
                            <small class="alert alert-warning">I you don't want to change password. Please leave them empty</small>
                          </div>
                          <div class="line"></div>
                          <div class="form-group row">
                            <label class="col-sm-3 form-control-label">Confirm Password</label>
                            <div class="col-sm-9">
                              <input type="password" class="form-control" name="password_confirmation" id="password_confirmation">
                            </div>
                          </div>
                           
                        </div>
                        <div class="col-6">
                           <div class="form-group row">
                              <label class="col-sm-2 form-control-label">Gender</label>
                              <div class="col-sm-10">
                                
                                <div class="i-checks" style="display: inline-block;">
                                  <input id="radioCustom1" type="radio" checked="" value="male" name="gender" class="form-control-custom radio-custom" {{ $user->gender == 'male' ? 'checked' : '' }}>
                                  <label for="radioCustom1">Male </label>
                                </div>
                                <div class="i-checks" style="display: inline-block;">
                                  <input id="radioCustom2" type="radio" value="female" name="gender" class="form-control-custom radio-custom" {{ $user->gender == 'female' ? 'checked' : '' }}>
                                  <label for="radioCustom2"> Female</label>
                                </div>
                                
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-sm-2 form-control-label">Photo</label>
                              <div class="col-sm-10">
                                <div class="form-group">
                                   <div class="input-group">
                                      <span class="input-group-btn">
                                          <span class="btn btn-default btn-file custom-file-upload">
                                              Browse… <input type="file" id="imgInp" name="photo">
                                          </span>
                                      </span>
                                      <input type="text" class="form-control" readonly>
                                  </div>
                                  <div class="img-priview mt-2">
                                    @if($user->photo)
                                    <img src="{!! url('/').'/uploads/users/'.$user->photo !!}" id='img-upload'/>
                                    @else
                                    <img src="{!! url('/').'/uploads/users/' !!}" id='img-upload'/>
                                    @endif
                                  </div>
                                </div>
                              </div>
                            </div>
                        </div>
                      </div>
                        <button type="submit" class="btn btn-primary float-right">Save changes</button>
                    </form>
                </div>
                <!-- /.Profile -->
                <div role="tabpanel" class="tab-pane fade in" id="alternative">
                    <ul class="list-group media-list media-list-stream">
                        <p>Test</p>
                    </ul>
                </div>
                 <div role="tabpanel" class="tab-pane fade in" id="link">
                    <ul class="list-group media-list media-list-stream">
                        <p>Link</p>
                    </ul>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection

@section('pages_scripts')
        <script src="{{ asset('assets/user/js/jquery.cookie.js') }}"> </script>
        <script src="{{ asset('assets/user/js/grasp_mobile_progress_circle-1.0.0.min.js') }}"></script>
        <script src="{{ asset('assets/user/js/jquery.nicescroll.min.js') }}"></script>
        <script src="{{ asset('assets/user/js/jquery.validate.min.js') }}"></script>
        <script src="{{ asset('assets/user/js/front.js') }}"></script>
<script type="text/javascript">
  $(document).ready( function() {
      $(document).on('change', '.btn-file :file', function() {
    var input = $(this),
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [label]);
    });

    $('.btn-file :file').on('fileselect', function(event, label) {
        
        var input = $(this).parents('.input-group').find(':text'),
            log = label;
        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
      
    });
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#img-upload').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInp").change(function(){
        readURL(this);
    });   
  });
</script>
        
@endsection