<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;

class RegistrationController extends Controller
{
    
    public function register(){
    	return view('authentication.register');
    }

    public function postRegister(Request $request){
    	try{
    		$request->validate([
              'first_name'       => 'required|min:3',
              'last_name'        => 'required|min:3',
              'email'            => 'required|email|unique:users',
              'password'         => 'required|between:3,32',
              'password_confirmation' => 'required|same:password',

          ]);

	    	$user = Sentinel::registerAndActivate($request->all());
	    	$role = Sentinel::findRoleBySlug('user');
	    	$role->users()->attach($user);

	    	session()->flash('msg', 'Registration successfully  completed!');
	    	return redirect('/login');

	    }catch(\Illuminate\Database\QueryException $ex){
	    	return redirect('register')->with(['error' => " Error Occured!"]);

	    }
    	
    }

}
