<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
class LoginController extends Controller
{
    public function login(){
    	return view('authentication.login');
    }

    public function postLogin(Request $request){
    	try{

            $rememberMe = false;
            if(isset($request->remember_me))
                $rememberMe = true;

            if (Sentinel::authenticate($request->all(), $rememberMe)) {
                    if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == 'admin'){
                        return redirect('admin/');
                    }elseif(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == 'user') {
                        return redirect('user/');
                    }
            }else {
                        // not authenticated
                        return redirect()->back()->with(['error' => 'Wrong credentials.']);
                    }
        }catch(ThrottlingException $e){

            $delay = $e->getDelay();
            return redirect()->back()->with(['error' => "You are banned for $delay seconds."]);
        }catch(NotActivatedException $e){

            return redirect('/login')->with(['error' => "Your account has not been activated yet."]);
        }
    }

    public function logout(){
        Sentinel::logout();
        return redirect('/login');
    }
}
