<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use DB;
use App\User;
use File;
use Hash;
use Validator;
use Redirect;

class UserController extends Controller
{
    
    public function index(){
    	if($user = Sentinel::getUser()){
            return view('user.index');
        }
    }

    public function profile(Request $request){
              
            if ($user = Sentinel::getUser())
                {
                    return view('user.profile')->with(['user' => $user]);
                }
    }

    public function updateProfile(Request $request){
       
        if ($user = Sentinel::getUser()) {
            $user_id = $user->id;

            if ($request->has('photo')) {
                $rules = array(
                    'photo' => 'image|mimes:jpg,jpeg,bmp,png',
                );
                $validator = Validator::make($request->only('photo'), $rules);

            // if ($validator->fails()) {
            //     return redirect::back()
            //         ->withInput()->withErrors($validator);
            // }
            }

            $users = User::find($user_id);
            $users->first_name = $request->first_name;
            $users->last_name = $request->last_name;
            $users->email = $request->email;
            $users->gender = $request->gender;
            $users->photo = $request->photo;
            if ($password = $request->has('password')) {
                $users->password = Hash::make($request->password);
            }
            if ($file = $request->file('photo')) {
                $extension = $file->getClientOriginalExtension() ?: 'png';
                $folderName = '/uploads/users/';
                $destinationPath = public_path() . $folderName;
                $safeName = str_random(10) . '.' . $extension;
                $file->move($destinationPath, $safeName);
                //delete old photo if exists
                if (File::exists(public_path() . $folderName . $user->photo)) {
                    File::delete(public_path() . $folderName . $user->photo);
                }

                //save new file path into db
                $user->photo = $safeName;

            }
            $users->save();

            return redirect('user/profile')->with('success', 'Successfully Changed');
        }
    }
}
